package com.example.liabanken.company;

import com.example.liabanken.exception.CompanyNotFoundException;
import com.example.liabanken.model.CompanyEntity;
import com.example.liabanken.model.dto.CompanyDTO;
import com.example.liabanken.model.dto.SchoolDTO;
import com.example.liabanken.repository.AddressRepository;
import com.example.liabanken.repository.CompanyRepository;
import com.example.liabanken.repository.SchoolRepository;
import com.example.liabanken.repository.UsersRepository;
import lombok.SneakyThrows;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TestCompanyController {
    @LocalServerPort
    int port;

    @Autowired
    private MockMvc mvc;

    @MockBean
    SchoolRepository schoolRepository;

    @MockBean
    UsersRepository usersRepository;

    @MockBean
    CompanyRepository companyRepository;

    @MockBean
    AddressRepository addressRepository;

    CompanyEntity companyEntity;
/*    CompanyDTO companyDto;
    SchoolDTO schoolDTO;*/

    @BeforeEach
    @Transactional
    void setUp() {
    /*   AddressEntity addressEntity= new AddressEntity("googleStreet2","900918","City");
        CompanyEntity companyEntity1= new CompanyEntity("111","Email","password",
                Role.COMPANYREPRESENTATIVE,"Google2","12345",addressEntity,
                false,new ArrayList<>());
        addressRepository.save(addressEntity);
        companyRepository.save(companyEntity1);
        companyDto1 = CompanyDto.from(companyEntity1);

        CompanyEntity companyEntity2= new CompanyEntity("222","Email","password",
                Role.COMPANYREPRESENTATIVE,"Google3","67890",addressEntity,
                false,new ArrayList<>());
        companyRepository.save(companyEntity2);
        companyDto2= CompanyDto.from(companyEntity2);*/
    }

    @AfterEach
    void tearDown() {
        companyRepository.deleteAll();
        usersRepository.deleteAll();
        addressRepository.deleteAll();
    }

    @Test
    void get_all_approved_companies() throws Exception {
        //When
        mvc.perform(get("/company/all-approved-companies"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
        when(companyRepository.findAll()).thenReturn((List<CompanyEntity>) companyEntity);

        //Then
        verify(companyRepository).findAll();
    }

/*    @Test
    void get_company_by_id_fail_because_not_found() {
        //When
        WebClient webClient = WebClient.create("http://localhost:" + port + "/company/companyById/333");

        //Then
        WebClientResponseException webClientResponseException = assertThrows(WebClientResponseException.class,
                () -> webClient.get()
                        .retrieve()
                        .bodyToMono(CompanyDTO.class)
                        .block());

        assertEquals("Not Found", webClientResponseException.getStatusText());
    }*/


    @Test
    @Ignore
    void create_company_success() {
    }

    @Test
    @Ignore
    void create_company_failed_because_already_exist() {
    }

    @Test
    @Ignore
    void add_company_advert_success() {
    }

    @Test
    @Ignore
    void create_company_password_success() {
    }

    @Test
    @Ignore
    void update_company_profile_success() {
    }

    @Test
    @Ignore
    void remove_company_success() {
    }


}
