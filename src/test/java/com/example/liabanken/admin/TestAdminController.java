package com.example.liabanken.admin;

import com.example.liabanken.exception.SchoolNotFoundException;
import com.example.liabanken.exception.UserNotFoundException;
import com.example.liabanken.jwt.KeyCloakToken;
import com.example.liabanken.model.*;
import com.example.liabanken.model.dto.AdminDTO;
import com.example.liabanken.model.dto.SchoolDTO;
import com.example.liabanken.repository.*;
import com.example.liabanken.security.TokenJwt;
import com.example.liabanken.service.AdminService;
import com.example.liabanken.service.SchoolService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TestAdminController {
    @LocalServerPort
    int port;

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    AdminService adminService;

    @Autowired
    SchoolService schoolService;

   // TokenJwt token;
  //  KeyCloakToken token;
    String token;
    WebClient webClient;


    @BeforeEach
    void setUp() {
        schoolRepository.deleteAll();
        adminRepository.deleteAll();
        usersRepository.deleteAll();
        addressRepository.deleteAll();
        token = adminService.getJwsToken();
        webClient = WebClient.builder()
                .baseUrl("http://localhost:8080")
                .defaultHeader("Authorization", "Bearer " + token)
                .build();
    }

    @AfterEach
    void tearDown() {
    }
/*
    @Test
    void test_create_admin_success() {
        //When

        AdminDTO adminUser = webClient
                .post()
                .uri("/admin/create-admin" + "?email=email&password=password&firstName=firstname&lastName=lastname")
                .retrieve()
                .bodyToMono(AdminDTO.class)
                .block();

        //Then
        assertThat(adminUser.getId(), is(notNullValue()));
        assertEquals("email", adminUser.getEmail());
        assertEquals("firstname", adminUser.getFirstName());
        assertEquals("lastname", adminUser.getLastName());

    }

    @Test
    void test_get_admin_by_id_fail_because_not_found() {
        //Given
        String id = UUID.randomUUID().toString();

        //When
        WebClientResponseException webClientResponseException = assertThrows(WebClientResponseException.class, () ->
                WebClient.create(getUrl("/admin/" + id))
                        .get()
                        .retrieve()
                        .bodyToMono(AdminDTO.class)
                        .block());

        UserNotFoundException userNotFoundException = assertThrows(UserNotFoundException.class, () -> {
            adminService.getAdminById(id);
        });

        //Then
        assertEquals(404, webClientResponseException.getRawStatusCode());
        assertEquals(userNotFoundException.getMessage(), id);
    }*/

/*    @Test
    void test_get_all_admin() {
        // Given
        AddressEntity addressEntity = new AddressEntity(UUID.randomUUID().toString(), "", "", "");
        UserEntity userEntity = new UserEntity(UUID.randomUUID().toString(), "email", "password", Role.ADMIN);
        List<AdminEntity> adminCreate = IntStream
                .range(0, 10)
                .boxed()
                .map(e -> {
                    AdminEntity adminEntity1 = new AdminEntity(UUID.randomUUID().toString(), "firstname", "lastname", userEntity, addressEntity);
                    usersRepository.save(userEntity);
                    addressRepository.save(addressEntity);
                    return adminRepository.save(adminEntity1);
                })
                .collect(Collectors.toList());

        // When
        List<AdminDTO> adminUsersList = WebClient.create(getUrl("/admin/all"))
                .get()
                .retrieve()
                .bodyToFlux(AdminDTO.class)
                .collectList()
                .block();

        // Then
        assertEquals(10, adminUsersList.size());
        assertEquals(10, adminCreate.size());
    }*/
/*
    @Test
    void test_get_admin_by_id() {
        // Given
        AddressEntity addressEntity = new AddressEntity("", "", "", "");
        UserEntity userEntity = new UserEntity(UUID.randomUUID().toString(), "email", "password", Role.ADMIN);
        AdminEntity adminEntity = new AdminEntity(UUID.randomUUID().toString(), "firstname", "lastname", userEntity, addressEntity);
        addressRepository.save(addressEntity);
        usersRepository.save(userEntity);
        adminRepository.save(adminEntity);

        //When


        AdminDTO useAdminBYId = WebClient
                .create(getUrl("/admin/") + adminEntity.getId())
                .get()
                .retrieve()
                .bodyToMono(AdminDTO.class)
                .block();

        //  Then
        assertThat(useAdminBYId, is(notNullValue()));
        assertEquals(adminEntity.getId(), useAdminBYId.getId());
        assertEquals(adminEntity.getFirstName(), useAdminBYId.getFirstName());
    }*/

 /*   @Test
    void test_get_all_school_requests() {
        //Given
        List<SchoolEntity> schoolEntities = IntStream
                .range(0, 10)
                .boxed()
                .map(integer -> {
                    return schoolService.createSchool("email",
                            "schoolName",
                            "orgNumber",
                            "street",
                            "zipcode",
                            "city");
                }).collect(Collectors.toList());

        //When
        List<SchoolDTO> getSchoolListByClient = WebClient
                .create(getUrl("/admin/all-school-requests"))
                .get()
                .retrieve()
                .bodyToFlux(SchoolDTO.class)
                .collectList()
                .block();

        //Then
        assertThat(getSchoolListByClient, is(notNullValue()));
        assertEquals(schoolEntities.size(), getSchoolListByClient.size());
    }

    @Test
    void test_add_school_request_by_id_success() throws IllegalAccessException, SchoolNotFoundException {
        //Given
        SchoolEntity schoolCreated = schoolService.createSchool("email",
                "schoolName",
                "orgNumber",
                "street",
                "zipcode",
                "city");

        //When
        SchoolDTO addSchoolRequestCreated = WebClient
                .create(getUrl("/admin/approve-school-requests?schoolId=" + schoolCreated.getId()))
                .post()
                .retrieve()
                .bodyToMono(SchoolDTO.class)
                .block();

        SchoolEntity schoolEntity1 = schoolRepository.findById(addSchoolRequestCreated.getId())
                .orElseThrow(() -> new SchoolNotFoundException("not_found"));

        //Then
        assertThat(addSchoolRequestCreated.getId(), is(notNullValue()));
        assertEquals(schoolEntity1.getId(), addSchoolRequestCreated.getId());
    }

    @Test
    void test_delete_school_from_request_by_id_success() throws IllegalAccessException, SchoolNotFoundException {
        //Given
        SchoolEntity schoolCreated = schoolService.createSchool("email",
                "schoolName",
                "orgNumber",
                "street",
                "zipcode",
                "city");

        //When
        String idDeleted = WebClient
                .create(getUrl("/admin/delete-school-requests?schoolId=" + schoolCreated.getId()))
                .delete()
                .retrieve()
                .bodyToMono(String.class)
                .block();

        //Then
        assertThat(schoolCreated, is(notNullValue()));
        assertEquals(schoolCreated.getId(), idDeleted);
    }

    private String getUrl(String url) {
        return "http://localhost:" + port + url;
    }
*/



}

