package com.example.liabanken.school;

import com.example.liabanken.exception.CompanyNotFoundException;
import com.example.liabanken.exception.SchoolNotFoundException;
import com.example.liabanken.model.*;
import com.example.liabanken.repository.*;
import com.example.liabanken.service.AdminService;
import com.example.liabanken.service.CompanyService;
import com.example.liabanken.service.SchoolService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Optional.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TestSchoolController {
    @LocalServerPort
    int port;

    @MockBean
    SchoolRepository schoolRepository;

    @MockBean
    AddressRepository addressRepository;

    @MockBean
    UsersRepository usersRepository;

    @MockBean
    CompanyRepository companyRepository;

    @MockBean
    StudentRepository studentRepository;

    @Autowired
    SchoolService schoolService;

    @Autowired
    CompanyService companyService;

    @Autowired
    AdminService adminService;

    SchoolEntity schoolEntity;
    CompanyEntity companyEntity;

    @BeforeEach
    @Transactional
    void setUp() {
        companyEntity = createCompany();
        schoolEntity = createSchool();
    }

    @AfterEach
    void tearDown() {
        schoolRepository.deleteAll();
        companyRepository.deleteAll();
        studentRepository.deleteAll();
        usersRepository.deleteAll();
        addressRepository.deleteAll();
    }

    private CompanyEntity createCompany() {
        AddressEntity addressCompanyEntity = new AddressEntity(UUID.randomUUID().toString(), "street", "zipcode", "city");
        UserEntity userCompanyEntity = new UserEntity(UUID.randomUUID().toString(), "email", "password", Role.COMPANYREPRESENTATIVE);
        CompanyEntity companyEntity = new CompanyEntity(UUID.randomUUID().toString(), "companyName", "orgNumber", addressCompanyEntity, userCompanyEntity, new ArrayList<>(), false, new ArrayList<>());
        return companyEntity;
    }

    private SchoolEntity createSchool() {
        AddressEntity addressSchoolEntity = new AddressEntity(UUID.randomUUID().toString(), "street", "zipcode", "city");
        UserEntity userSchoolEntity = new UserEntity(UUID.randomUUID().toString(), "email", "password", Role.SCHOOLREPRESENTATIVE);
        SchoolEntity schoolEntity = new SchoolEntity(UUID.randomUUID().toString(), "schoolName", "orgNumber", userSchoolEntity, addressSchoolEntity, new ArrayList<>(), new ArrayList<>(), false);
        return schoolEntity;
    }

    @Test
    void test_create_school_request_success() {
        //Given
        ArgumentCaptor<SchoolEntity> schoolEntityArgumentCaptor = ArgumentCaptor.forClass(SchoolEntity.class);
        when(addressRepository.save(anyObject())).then(invocationOnMock -> invocationOnMock.getArgument(0));
        when(usersRepository.save(anyObject())).then(invocationOnMock -> invocationOnMock.getArgument(0));
        when(schoolRepository.save(anyObject())).then(invocationOnMock -> invocationOnMock.getArgument(0));

        //When
        SchoolEntity school = schoolService.createSchool("email", "schoolName", "orgNumber", "street", "zipcode", "city");

        //Then
        verify(schoolRepository).save(schoolEntityArgumentCaptor.capture());
        verify(usersRepository).save(school.getUserEntity());
        verify(addressRepository).save(school.getAddress());

        assertThat(school, is(notNullValue()));
        assertEquals(school.getSchoolName(), "schoolName");
        assertEquals(schoolEntityArgumentCaptor.getValue(), school);
    }


    @Test
    void test_create_student() throws SchoolNotFoundException {
        when(schoolRepository.findById(anyString())).thenReturn(Optional.ofNullable(schoolEntity));

        schoolEntity.setApproved(true);
        SchoolEntity schoolEntity1 = schoolService.createStudent(this.schoolEntity.getId(), "", "studentname", "studentlastname", "email", "street", "zipcode", "city");

        verify(schoolRepository).save(schoolEntity1);

        assertEquals(this.schoolEntity, schoolEntity1);
    }


    @Test
    void test_create_password_success() {
        //Given
        when(usersRepository.save(anyObject())).then(invocationOnMock -> invocationOnMock.getArgument(0));
        schoolEntity = schoolService.createSchool("email", "schoolName", "orgNumber", "street", "zipcode", "city");
        when(schoolRepository.findById(anyString())).thenReturn(Optional.of(schoolEntity));

        //When
        SchoolEntity schoolEntityPasswordChanged = schoolService.createPassword(schoolEntity.getId(), schoolEntity.getUserEntity().getEmail(), "newPassword");

        //Then
        assertEquals(schoolEntity, schoolEntityPasswordChanged);
        assertThat(schoolEntity, is(notNullValue()));
    }

    @Test
    void test_get_school_by_id_success() throws SchoolNotFoundException {
        //Given
        SchoolEntity school = schoolService.createSchool("email", "schoolName", "orgNumber", "street", "zipcode", "city");
        when(schoolRepository.findById(anyString())).thenReturn(Optional.of(school));

        //When
        SchoolEntity schoolEntity1 = schoolService.getSchoolRequestById(school.getId());

        //Then
        verify(schoolRepository).findById(schoolEntity1.getId());
        assertNotNull(school.getId());
    }

    @Test
    void test_get_all_approved_schools_success() {
        //Given
        ArgumentCaptor<SchoolEntity[]> argumentCaptor = ArgumentCaptor.forClass(SchoolEntity[].class);

        List<SchoolEntity> schoolEntityList = IntStream.range(0, 10).boxed().map((i) -> {
            schoolEntity = schoolService.createSchool("email", "schoolName", "orgNumber", "street", "zipcode", "city");
            when(schoolRepository.findById(anyString())).thenReturn(Optional.ofNullable((schoolEntity)));
            try {
                SchoolEntity schoolEntity = schoolService.approveSchoolById(this.schoolEntity.getId());
                return schoolEntity;
            } catch (SchoolNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }).collect(Collectors.toList());

        //When
        when(schoolRepository.findAll()).thenReturn(schoolEntityList);

        List<SchoolEntity> allApprovedSchools = schoolService.getAllApprovedSchools().collect(Collectors.toList());

        //Then
        verify(schoolRepository).findAll();
        assertEquals(schoolEntityList, allApprovedSchools);
    }

    @Test
    void test_approve_company_by_school_success() throws SchoolNotFoundException, CompanyNotFoundException {
        //Given
        when(schoolRepository.findById(anyString())).thenReturn(Optional.ofNullable(schoolEntity));
        when(companyRepository.findById(anyString())).thenReturn(Optional.ofNullable(companyEntity));
        schoolEntity = schoolService.createSchool("email", "schoolName", "orgNumber", "street1", "zipcode", "city");
        companyService.createCompany("email", "password", "companyName", "orhNumber", "street", "zipcode", "city");

        //When
        schoolService.approveCompanyBySchoolId(schoolEntity.getId(), companyEntity.getId());

        //Then
        verify(schoolRepository).save(schoolEntity);
        verify(companyRepository).save(companyEntity);
        verify(schoolRepository, VerificationModeFactory.times(2)).save(any());

        assertTrue(companyEntity.isApprove());
    }

    @Test
    void test_delete_approved_company_success() throws CompanyNotFoundException, SchoolNotFoundException {
        //Given
        when(schoolRepository.findById(anyString())).thenReturn(Optional.ofNullable(schoolEntity));
        when(companyRepository.findById(anyString())).thenReturn(Optional.ofNullable(companyEntity));
        schoolEntity.addCompany(companyEntity);

        //When
        schoolService.removeCompanyFromSchool(schoolEntity.getId(), companyEntity.getId());
        boolean exist = schoolEntity.getCompanies()
                .stream()
                .anyMatch(companyEntity1 -> companyEntity1.getId().equals(companyEntity.getId()));

        //Then
        verify(companyRepository).save(companyEntity);
        verify(schoolRepository).save(schoolEntity);

        assertFalse(exist);
    }

    private String getUrl(String url) {
        return "http://localhost:" + port + "/school/" + url;
    }

}

