package com.example.liabanken.student;

import com.example.liabanken.config.ConvertEntityToDTOs;
import com.example.liabanken.exception.SchoolNotFoundException;
import com.example.liabanken.exception.StudentNotFoundException;
import com.example.liabanken.model.*;
import com.example.liabanken.model.dto.AdminDTO;
import com.example.liabanken.model.dto.SchoolDTO;
import com.example.liabanken.model.dto.StudentDTO;
import com.example.liabanken.repository.*;
import com.example.liabanken.service.AdminService;
import com.example.liabanken.service.SchoolService;
import com.example.liabanken.service.StudentService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TestStudentController {
    @LocalServerPort
    int port;

    @MockBean
    AdminRepository adminRepository;

    @MockBean
    SchoolRepository schoolRepository;

    @MockBean
    AddressRepository addressRepository;

    @MockBean
    UsersRepository usersRepository;

    @MockBean
    StudentRepository studentRepository;

    @Autowired
    AdminService adminService;

    @Autowired
    StudentService studentService;

    @Autowired
    SchoolService schoolService;

    SchoolEntity schoolEntity;
    StudentEntity studentEntity;
    UserEntity userEntity;
    AddressEntity addressEntity;

    @BeforeEach
    @Transactional
    void setUp() {
        createSchool();
        createStudent();
    }

    @AfterEach
    void tearDown() {
        studentRepository.deleteAll();
        schoolRepository.deleteAll();
        adminRepository.deleteAll();
        usersRepository.deleteAll();
        addressRepository.deleteAll();
    }

    @Test
    void test_get_student_by_id_success() {
        //Given
        when(studentRepository.findById(anyString())).thenReturn(Optional.of(studentEntity));

        //When
        StudentDTO studentDTO2 = ConvertEntityToDTOs.convertStudentEntityToDTO(studentEntity);

        StudentDTO studentGetById = WebClient
                .create(getUrl("/student/") + studentEntity.getId())
                .get()
                .retrieve()
                .bodyToMono(StudentDTO.class)
                .block();

        //Given
        assertEquals(studentGetById, studentDTO2);
    }

 /*   @Test
    void test_get_all_student_success() {
        // Given
        when(studentRepository.findAll()).thenReturn(List.of(createStudent(), createStudent(), createStudent()));

        // When
        List<StudentDTO> studentList = WebClient.create(getUrl("/student/all"))
                .get()
                .retrieve()
                .bodyToFlux(StudentDTO.class)
                .collectList()
                .block();

        //Then
        assertEquals(3, studentList.size());
    }*/

   /* @Test
    @Ignore
    void test_get_all_approved_companies_success() {
    }

    @Test
    @Ignore
    void test_add_favorite_success() {
    }

    @Test
    @Ignore
    void test_remove_favorite_success() {
    }

    @Test
    @Ignore
    void test_get_all_favorites_by_id_success() {
    }

    @Test
    @Ignore
    void test_get_all_adverts_success() {
    }

    @Test
    @Ignore
    void test_search_adverts_success() {
    }

    @Test
    @Ignore
    void test_create_password_success() {
    }

    @Test
    @Ignore
    void test_remove_student_by_id_success() {
    }*/

    private String getUrl(String url) {
        return "http://localhost:" + port + url;
    }

    public SchoolEntity createSchool() {
        userEntity = new UserEntity(UUID.randomUUID().toString(), "", "", Role.SCHOOLREPRESENTATIVE);
        addressEntity = new AddressEntity(UUID.randomUUID().toString(), "", "", "");
        schoolEntity = new SchoolEntity(UUID.randomUUID().toString(), "", "", userEntity, addressEntity, new ArrayList<>(), new ArrayList<>(), true);
        return schoolEntity;
    }

    public StudentEntity createStudent() {
        userEntity = new UserEntity(UUID.randomUUID().toString(), "", "", Role.SCHOOLREPRESENTATIVE);
        addressEntity = new AddressEntity(UUID.randomUUID().toString(), "", "", "");
        studentEntity = new StudentEntity(UUID.randomUUID().toString(), "", "","", userEntity, addressEntity, schoolEntity, new ArrayList<>());
        return studentEntity;
    }

}
