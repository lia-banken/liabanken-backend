
package com.example.liabanken;

import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;


@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {
    @Autowired
    public void configureGlobal(
            AuthenticationManagerBuilder auth) throws Exception {

        KeycloakAuthenticationProvider keycloakAuthenticationProvider
                = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(
                new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    public KeycloakSpringBootConfigResolver KeycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(
                new SessionRegistryImpl());
    }


     /*   http
                .csrf().disable().authorizeRequests().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
                "/swagger-ui.html", "/webjars/**", "/swagger-resources/configuration/ui", "/swagger-ui.html",
                "/swagger-resources/configuration/security")
                .permitAll();*/

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.cors();
        http.authorizeRequests()
                // .addResourceHandler("/swagger/**")
                //  .addResourceLocations("classpath:swagger/")
                .antMatchers("/swagger-ui/*", "/v3/api-docs").permitAll()
                .antMatchers("/v3/api-docs/swagger-config").permitAll()
                // .antMatchers("/swagger-config").permitAll()
                //  .antMatchers("/v3/api-docs/swagger-config").permitAll()

                .antMatchers("/admin/createJsonWebToken").permitAll()
                .antMatchers("/admin/register_new_admin_user").permitAll()
               // .antMatchers("/admin/g").permitAll()
                .antMatchers("/company/*").permitAll()
                .antMatchers("/school/*").permitAll()
                .antMatchers("/student/*").permitAll()

                .antMatchers("/admin/*").hasRole("admin")
                .antMatchers("/admin/*").authenticated()

                .antMatchers("/school/all-approved-schools").authenticated()
                .antMatchers("/school/remove-school/").authenticated()
                .antMatchers("/get-approve-school-by-id/{schoolId}").authenticated()
                .anyRequest().permitAll();
        http.csrf().disable();
    }
}


