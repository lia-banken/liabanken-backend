package com.example.liabanken.config;
import com.example.liabanken.model.*;
import com.example.liabanken.model.dto.*;
import java.util.stream.Collectors;
public class ConvertEntityToDTOs {

    //user f
    public static UserDTO convertUserDto(UserEntity userEntity) {
        return new UserDTO(
                userEntity.getId(),
                userEntity.getEmail(),
                userEntity.getPassword(),
                userEntity.getRole()
        );
    }

    //Admin
    public static AdminDTO convertAdminDTO(AdminEntity adminEntity) {
        return new AdminDTO(
                adminEntity.getId(),
                adminEntity.getUser().getEmail(),
                adminEntity.getFirstName(),
                adminEntity.getLastName(),
                convertUserDto(adminEntity.getUser()),
                convertAddressEntityToDto(adminEntity.getAddress()));
    }

    //School
    public static SchoolDTO convertSchoolEntityToDTO(SchoolEntity schoolEntity) {
        SchoolDTO schoolDTO = new SchoolDTO(
                schoolEntity.getId(),
                schoolEntity.getUserEntity().getEmail(),
                schoolEntity.getSchoolName(),
                schoolEntity.getOrganizationNumber(),
                convertUserDto(schoolEntity.getUserEntity()),
                convertAddressEntityToDto(schoolEntity.getAddress()),
                schoolEntity.isApproved(),
                schoolEntity.getCompanies()
                        .stream()
                        .map(ConvertEntityToDTOs::convertCompanyEntityToDTO)
                        .collect(Collectors.toList()),
                schoolEntity.getStudentEntities()
                        .stream()
                        .map(ConvertEntityToDTOs::convertStudentEntityToDTO)
                        .collect(Collectors.toList()));
        return schoolDTO;
    }

    //Student
    public static StudentDTO convertStudentEntityToDTO(StudentEntity studentEntity) {
        StudentDTO studentDto = new StudentDTO(
                studentEntity.getId(),
                studentEntity.getFirstName(),
                studentEntity.getLastName(),
               // convertUserDto(studentEntity.getUserEntity()),
                convertAddressEntityToDto(studentEntity.getAddress()),
                studentEntity.getFavorites()
                        .stream()
                        .map(ConvertEntityToDTOs::convertAdvertEntityToDto).collect(Collectors.toList())
        );
        return studentDto;
    }

    //Company
    public static CompanyDTO convertCompanyEntityToDTO(CompanyEntity companyEntity) {
        CompanyDTO companyDto = new CompanyDTO(
                companyEntity.getId(),
                companyEntity.getUserEntity().getEmail(),
                companyEntity.getCompanyName(),
                companyEntity.getOrganizationNumber(),
                convertUserDto(companyEntity.getUserEntity()),
                convertAddressEntityToDto(companyEntity.getAddress()),
                companyEntity.isApprove(),
                companyEntity.getListOfAdverts()
                        .stream()
                        .map(ConvertEntityToDTOs::convertAdvertEntityToDto).collect(Collectors.toList()));
        return companyDto;
    }

    public static AdvertDTO convertAdvertEntityToDto(AdvertEntity advertEntity) {
        return new AdvertDTO(
                advertEntity.getId(),
                advertEntity.getName(),
                advertEntity.getDescription(),
                advertEntity.getArea(),
                advertEntity.getProfession(),
                advertEntity.isFav()
        );
    }

    public static AddressDTO convertAddressEntityToDto(AddressEntity addressEntity) {
        return new AddressDTO(
                addressEntity.getId(),
                addressEntity.getStreet(),
                addressEntity.getZipCode(),
                addressEntity.getCity()
        );
    }
}
