
package com.example.liabanken.keycloakCreateUser;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class KeycloakAdminClientService {

    private final Keycloak keycloak;

    @Autowired
    public KeycloakAdminClientService(Keycloak keycloak) {
        this.keycloak = keycloak;
    }

    public UserRepresentation createKeycloakUser(String firstName, String lastName, String email, String userName, String password) {
        RealmResource realm = keycloak.realm("test");//You can choose any realm you want your user to be in.
        UserRepresentation user = createUser(firstName, lastName, email, userName, password);
        UsersResource users = realm.users();
        Response response = users.create(user);

        //Find the created user by email
        //set the created users ID
        if (response.getStatus() == 201) {
            List<UserRepresentation> all = users.search(user.getUsername());
            Optional<UserRepresentation> found = all.stream().filter(any -> any.getEmail().equalsIgnoreCase(user.getEmail())).findFirst();
            if (found.isEmpty())
                return null;

            user.setId(found.get().getId());
        }


        return response.getStatus() == 201 ? user : null;
    }

    public UserRepresentation createUser(String firstName, String lastName, String email, String userName, String password) {
        UserRepresentation user = new UserRepresentation();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setEnabled(true);
        user.setUsername(userName);
        user.setCredentials(Collections.singletonList(createPasswordCredentials(password)));

        return user;
    }

    private static CredentialRepresentation createPasswordCredentials(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();

        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);
        passwordCredentials.setTemporary(false);

        return passwordCredentials;
    }
}

