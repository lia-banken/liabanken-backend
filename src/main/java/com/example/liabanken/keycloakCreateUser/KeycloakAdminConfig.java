package com.example.liabanken.keycloakCreateUser;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeycloakAdminConfig {

    static Keycloak keycloakAdmin = null;
    final static String serverUrl = "http://localhost:8000/auth";
    final static String realm = "master";
    final static String clientId = "admin-cli";
    final static String clientSecret = "e0c8d308-34ec-433a-8196-f45038ee1928";

    //if you want to builde KeycloakBuilder With PasswordCredentials instead of CLIENT_CREDENTIALS use:g
    //final static String userName = "admin";
    //final static String password = "password";
    @Bean
    public Keycloak getInstance() {
        if (keycloakAdmin == null) {
            keycloakAdmin = KeycloakBuilder.builder()
                    .serverUrl(serverUrl)
                    .realm(realm)
                    .clientId(clientId)
                    .clientSecret(clientSecret)
                    .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                    //.username(userName)
                    //.password(password)
                    .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                    .build();
        }
        return keycloakAdmin;
    }
}
