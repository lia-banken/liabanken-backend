package com.example.liabanken.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "adverts")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdvertEntity {
    @Id
    String id;

    @Column(name = "advert_name")
    String name;

    @Column(name = "advert_description")
    String description;

    @Column(name = "advert_area")
    String area;

    @Column(name = "advert_profession")
    String profession;

    @Column(name = "favorite")
    boolean fav;
}


