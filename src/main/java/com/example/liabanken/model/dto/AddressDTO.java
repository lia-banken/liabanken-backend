package com.example.liabanken.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

@Value
public class AddressDTO {
    String id;
    String street;
    String zipCode;
    String city;

    @JsonCreator
    public AddressDTO(
            @JsonProperty("id") String id,
            @JsonProperty("street") String street,
            @JsonProperty("zipCode") String zipCode,
            @JsonProperty("city") String city) {
        this.id = id;
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
    }
}
