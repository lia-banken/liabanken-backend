package com.example.liabanken.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class AdvertDTO {
    String id;
    String name;
    String description;
    String area;
    String profession;
    boolean fav;

    @JsonCreator
    public AdvertDTO(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("area") String area,
            @JsonProperty("profession") String profession,
            @JsonProperty("fav") boolean fav) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.area = area;
        this.profession = profession;
        this.fav = fav;
    }
}
