package com.example.liabanken.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;


@Value
public class SchoolDTO {
    String id;
    String email;
    String schoolName;
    String organizationNumber;
    UserDTO userDto;
    AddressDTO address;
    boolean approved;
    List<CompanyDTO> companies;
    List<StudentDTO> studentSchoolEntities;

    @JsonCreator
    public SchoolDTO(@JsonProperty("id") String id,
                     @JsonProperty("email") String email,
                     @JsonProperty("schoolName") String schoolName,
                     @JsonProperty("organizationNumber") String organizationNumber,
                     @JsonProperty("userEntity") UserDTO userDto,
                     @JsonProperty("address") AddressDTO address,
                     @JsonProperty("approved") boolean approved,
                     @JsonProperty("companies") List<CompanyDTO> companies,
                     @JsonProperty("studentSchoolEntities") List<StudentDTO> studentSchoolEntities) {
        this.id = id;
        this.email = email;
        this.schoolName = schoolName;
        this.organizationNumber = organizationNumber;
        this.userDto = userDto;
        this.address = address;
        this.approved = approved;
        this.companies = companies;
        this.studentSchoolEntities = studentSchoolEntities;
    }
}
