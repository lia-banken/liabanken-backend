package com.example.liabanken.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class CompanyDTO {
    String id;
    String email;
    String companyName;
    String organizationNumber;
    UserDTO userDto;
    AddressDTO address;
    boolean approved;
    List<AdvertDTO> listOfAdverts;

    @JsonCreator
    public CompanyDTO(@JsonProperty("id") String id,
                      @JsonProperty("email") String email,
                      @JsonProperty("companyName") String companyName,
                      @JsonProperty("organizationNumber") String organizationNumber,
                      @JsonProperty("userEntity") UserDTO userDto,
                      @JsonProperty("address") AddressDTO address,
                      @JsonProperty("approved") Boolean approved,
                      @JsonProperty("listOfAdverts") List<AdvertDTO> listOfAdverts) {
        this.id = id;
        this.email = email;
        this.companyName = companyName;
        this.organizationNumber = organizationNumber;
        this.userDto = userDto;
        this.address = address;
        this.approved = approved;
        this.listOfAdverts = listOfAdverts;
    }
}
