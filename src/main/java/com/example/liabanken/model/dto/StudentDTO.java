package com.example.liabanken.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class StudentDTO {
    String id;
    String firstname;
    String lastname;
 //   UserDTO userDto;
    AddressDTO addressDTO;
    List<AdvertDTO> favorites;

    @JsonCreator
    public StudentDTO(
            @JsonProperty("id") String id,
            @JsonProperty("firstName") String firstname,
            @JsonProperty("lastName") String lastname,
           // @JsonProperty("userName") UserDTO userDto,
            @JsonProperty("addressDTO") AddressDTO addressDTO,
            @JsonProperty("favorites") List<AdvertDTO> favorites) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
     //   this.userDto = userDto;
        this.addressDTO = addressDTO;
        this.favorites = favorites;
    }
}
