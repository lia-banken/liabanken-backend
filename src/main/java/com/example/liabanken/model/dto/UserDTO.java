package com.example.liabanken.model.dto;

import com.example.liabanken.model.Role;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
public class UserDTO {
    String id;
    String email;
    String password;
    Role role;

    @JsonCreator
    public UserDTO(@JsonProperty("id") String id,
                   @JsonProperty("email") String email,
                   @JsonProperty("password") String password,
                   @JsonProperty("role") Role role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.role = role;
    }
}
