package com.example.liabanken.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class AdminDTO {
    String id;
    String email;
    String firstName;
    String lastName;
    UserDTO userDto;
    AddressDTO address;

    @JsonCreator
    public AdminDTO(@JsonProperty("id") String id,
                    @JsonProperty("email") String email,
                    @JsonProperty("firstName") String firstName,
                    @JsonProperty("lastName") String lastName,
                    @JsonProperty("userName") UserDTO userDto,
                    @JsonProperty("address") AddressDTO address) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userDto = userDto;
        this.address = address;
    }
}
