package com.example.liabanken.model;

public enum Role {
    ADMIN,
    STUDENT,
    COMPANYREPRESENTATIVE,
    SCHOOLREPRESENTATIVE
}
