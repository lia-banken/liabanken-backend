package com.example.liabanken.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "student")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentEntity {
    @Id
    String id;

    @Column(name = "student_keycloak")
    String keyCloakId;

    @Column(name = "student_firstname")
    String FirstName;

    @Column(name = "student_lastname")
    String LastName;

    @OneToOne
    @JoinColumn(name = "user_id")
    UserEntity userEntity;

    @OneToOne()
    @JoinColumn(name = "address_id")
    AddressEntity address;

    @ManyToOne
    @JoinColumn(name = "school_id")
    SchoolEntity schoolEntity;

    @OneToMany
    @JoinColumn(name = "favorites")
    List<AdvertEntity> favorites;

    public void addFavorite(AdvertEntity advertEntity) {
        favorites.add(advertEntity);
    }
}


