package com.example.liabanken.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "address")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AddressEntity {
    @Id
    String id;

    @Column(name = "street")
    String street;

    @Column(name = "zipcode")
    String zipCode;

    @Column(name = "city")
    String city;
}

