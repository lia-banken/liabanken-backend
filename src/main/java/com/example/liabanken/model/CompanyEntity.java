package com.example.liabanken.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "company")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CompanyEntity {
    @Id
    String id;

    @Column(name = "company_ame")
    String companyName;

    @Column(name = "organization_number")
    String organizationNumber;

    @OneToOne()
    @JoinColumn(name = "address_id")
    AddressEntity address;

    @OneToOne
    @JoinColumn(name = "user_id")
    UserEntity userEntity;

    @ManyToMany(mappedBy = "companies")
    @Column(name = "schools")
    List<SchoolEntity> schools = new ArrayList<>();

    @JoinColumn(name = "approved")
    boolean approve;

    @OneToMany()
    @JoinColumn(name = "adverts")
    List<AdvertEntity> listOfAdverts;

    public void addSchool(SchoolEntity schoolEntities) {
        schools.add(schoolEntities);
    }

    public void addAdvert(AdvertEntity advertEntity) {
        listOfAdverts.add(advertEntity);
    }

}
