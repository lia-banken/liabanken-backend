package com.example.liabanken.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "admins")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdminEntity {
    @Id
    String id;

    @Column(name = "admin_keycloak")
    String keyCloakId;

    @Column(name = "firstname")
    String firstName;

    @Column(name = "lastname")
    String lastName;

    @OneToOne
    @JoinColumn(name = "user_id")
    UserEntity user;

    @OneToOne()
    @JoinColumn(name = "address_id")
    AddressEntity address;

}
