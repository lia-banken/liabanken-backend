package com.example.liabanken.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "schools")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SchoolEntity {
    @Id
    String id;

    @Column(name = "school_name")
    String schoolName;

    @Column(name = "organization_number")
    String organizationNumber;

    @OneToOne
    @JoinColumn(name = "user_id")
    UserEntity userEntity;

    @OneToOne()
    @JoinColumn(name = "address_id")
    AddressEntity address;

    @OneToMany(mappedBy = "schoolEntity")
    @Column(name = "students")
    List<StudentEntity> studentEntities = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "companies_school_list",
            joinColumns = @JoinColumn(name = "school_id"),
            inverseJoinColumns = @JoinColumn(name = "company_id")
    )
    List<CompanyEntity> companies = new ArrayList<>();

    @Column(name = "approved")
    boolean approved;

    public void addCompany(CompanyEntity companyEntity) {
       companies.add(companyEntity);
    }

    public void addStudent(StudentEntity studentEntity) {
        studentEntities.add(studentEntity);
    }

    public void removeCompany(CompanyEntity companyEntity) {
        companies.remove(companyEntity);
    }

}
