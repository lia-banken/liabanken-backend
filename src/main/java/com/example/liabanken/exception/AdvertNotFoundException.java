package com.example.liabanken.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "ADVERT_NOT_FOUND")
public class AdvertNotFoundException extends Exception {
}
