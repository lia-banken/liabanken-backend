package com.example.liabanken.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "SCHOOL_NOT_FOUND")
public class SchoolNotFoundException extends Exception {
    public SchoolNotFoundException(String message) {
        super(message);
    }
}
