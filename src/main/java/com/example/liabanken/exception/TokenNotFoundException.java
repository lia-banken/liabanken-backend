package com.example.liabanken.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "TOKEN_NOT_FOUND")
public class TokenNotFoundException extends Exception{
    public TokenNotFoundException() {
    }
}
