package com.example.liabanken.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "ADDRESS_NOT_FOUND")
public class AddressNotFoundException extends Exception {
}
