package com.example.liabanken.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
public class CreateUserFailException extends Exception{
    public CreateUserFailException(String message) {
        super(message);
    }
}
