package com.example.liabanken.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "COMPANY_NOT_FOUND")
public class CompanyNotFoundException extends Exception {
    public CompanyNotFoundException(){
        super("COMPANY_NOT_FOUND");
    }
}
