package com.example.liabanken.service;

import com.example.liabanken.exception.AddressNotFoundException;
import com.example.liabanken.exception.CompanyNotFoundException;
import com.example.liabanken.exception.SchoolNotFoundException;
import com.example.liabanken.exception.StudentNotFoundException;
import com.example.liabanken.model.*;
import com.example.liabanken.repository.*;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class SchoolService {
    AdminRepository adminRepository;
    SchoolRepository schoolRepository;
    AddressRepository addressRepository;
    UsersRepository usersRepository;
    CompanyRepository companyRepository;
    StudentRepository studentRepository;
    //KeyCloakToken keyCloakToken;

    @SneakyThrows
    public SchoolEntity createSchool(String email,
                                     String schoolName,
                                     String organizationNumber,
                                     String street,
                                     String zipcode,
                                     String city) {
        AddressEntity addressEntity = new AddressEntity(UUID.randomUUID().toString(), street, zipcode, city);
        UserEntity userEntity = new UserEntity(UUID.randomUUID().toString(), email, "null", Role.SCHOOLREPRESENTATIVE);
        SchoolEntity schoolEntity = new SchoolEntity(
                UUID.randomUUID().toString(),
                schoolName,
                organizationNumber,
                userEntity,
                addressEntity,
                new ArrayList<>(),
                new ArrayList<>(),
                false);
        addressRepository.save(addressEntity);
        usersRepository.save(userEntity);
        schoolRepository.save(schoolEntity);
        return schoolEntity;
    }

    public SchoolEntity createPassword(String id, String email, String password) {
        SchoolEntity schoolEntity = schoolRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException("school with id " + id + "dose not exist"));

        if (password != null && password.length() > 0) {
            schoolEntity.getUserEntity().setPassword(password);
        }
        if (!schoolEntity.getUserEntity().getEmail().equals(email)) {
            throw new IllegalStateException(" wrong email");
        }

        schoolRepository.save(schoolEntity);
        return schoolEntity;
    }

    public SchoolEntity createStudent(String schoolId, String password, String firstname, String lastname, String email, String street, String zipcode, String city) throws SchoolNotFoundException {
        SchoolEntity schoolEntity = schoolRepository.findById(schoolId).orElseThrow(() -> new SchoolNotFoundException(schoolId));
        if (schoolEntity.isApproved() == false) {
            throw new IllegalStateException("skolan are inte gokänt");
        }

        UserEntity userEntity = new UserEntity(UUID.randomUUID().toString(), email, password, Role.STUDENT);
        AddressEntity addressEntity = new AddressEntity(UUID.randomUUID().toString(), street, zipcode, city);
        StudentEntity studentEntity = new StudentEntity(UUID.randomUUID().toString(), "a0fafb25-ff48-4fa5-9bce-2033d662bb6d", firstname, lastname, userEntity, addressEntity, schoolEntity, new ArrayList<>());
        schoolEntity.addStudent(studentEntity);
        usersRepository.save(userEntity);
        addressRepository.save(addressEntity);
        studentRepository.save(studentEntity);
        schoolRepository.save(schoolEntity);
        return schoolEntity;
    }
/*
    public Stream<SchoolEntity> getAllSchoolRequests(String principalId) {
        adminRepository.getAdminEntitiesByKeyCloakId(principalId);
        return schoolRepository.findAll().stream()
                .filter(schoolEntity -> !schoolEntity.isApproved());
    }*/

    public Stream<SchoolEntity> getAllApprovedSchools() {
        return schoolRepository.findAll().stream().filter(SchoolEntity::isApproved);
    }
     public SchoolEntity getApprovedSchoolById(String schoolId) throws SchoolNotFoundException {
        return schoolRepository.findById(schoolId).orElseThrow(() -> new SchoolNotFoundException(schoolId));
     }

    public SchoolEntity getSchoolRequestById(String id) throws SchoolNotFoundException {
        return schoolRepository.findById(id)
                .orElseThrow(() -> new SchoolNotFoundException(id));
    }
    public SchoolEntity getSchoolById(String id) throws SchoolNotFoundException {
        return schoolRepository.findById(id).filter(SchoolEntity::isApproved).orElseThrow(() -> new SchoolNotFoundException(id));
    }

    public Stream<StudentEntity> getAllStudentsBySchoolId(String schoolId) throws SchoolNotFoundException {
        SchoolEntity schoolEntity = schoolRepository.findById(schoolId).orElseThrow(() -> new SchoolNotFoundException(schoolId));
        return schoolEntity.getStudentEntities().stream();
    }

    public SchoolEntity approveCompanyBySchoolId(String schoolId, String companyId) throws SchoolNotFoundException, CompanyNotFoundException {
        SchoolEntity schoolEntity = schoolRepository.findById(schoolId).orElseThrow(() -> new SchoolNotFoundException(schoolId));

        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);

        companyEntity.setApprove(true);
        schoolEntity.addCompany(companyEntity);
        companyEntity.addSchool(schoolEntity);
        schoolRepository.save(schoolEntity);
        companyRepository.save(companyEntity);
        return schoolEntity;
    }

    public SchoolEntity approveSchoolById(String schoolId) throws SchoolNotFoundException {
        SchoolEntity schoolToAdd = schoolRepository.findById(schoolId)
                .orElseThrow(() -> new SchoolNotFoundException(schoolId));
        schoolToAdd.setApproved(true);
        schoolRepository.save(schoolToAdd);
        return schoolToAdd;
    }


    public SchoolEntity removeCompanyFromSchool(String schoolId, String companyId) throws SchoolNotFoundException, CompanyNotFoundException {
        SchoolEntity schoolEntity = schoolRepository.findById(schoolId).orElseThrow(() -> new SchoolNotFoundException(schoolId));
        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        schoolEntity.removeCompany(companyEntity);

        companyRepository.save(companyEntity);
        schoolRepository.save(schoolEntity);
        return schoolEntity;
    }

    public SchoolEntity removeStudentFromSchool(String schoolId, String studentId) throws SchoolNotFoundException, StudentNotFoundException {
        SchoolEntity schoolEntity = schoolRepository.findById(schoolId).orElseThrow(() -> new SchoolNotFoundException(schoolId));
        StudentEntity studentEntity = studentRepository.findById(studentId).orElseThrow(() -> new StudentNotFoundException(studentId));
        studentRepository.deleteById(studentId);
        usersRepository.deleteById(studentEntity.getUserEntity().getId());
        addressRepository.deleteById(studentEntity.getAddress().getId());
        return schoolEntity;
    }

    public String removeSchoolFromRequest(String schoolId) throws SchoolNotFoundException, AddressNotFoundException {
        SchoolEntity schoolToDelete = schoolRepository.findById(schoolId)
                .orElseThrow(() -> new SchoolNotFoundException(schoolId));
        schoolRepository.delete(schoolToDelete);
        addressRepository.delete(schoolToDelete.getAddress());
        usersRepository.delete(schoolToDelete.getUserEntity());
        return schoolToDelete.getId();
    }

    public String removeSchoolById(String schoolId) throws SchoolNotFoundException {
        SchoolEntity schoolEntity = schoolRepository.findById(schoolId).orElseThrow(() -> new SchoolNotFoundException(schoolId));
        schoolEntity.getStudentEntities().forEach(this::deleteAllStudentsFromSchool);
        schoolRepository.delete(schoolEntity);
        addressRepository.delete(schoolEntity.getAddress());
        usersRepository.delete(schoolEntity.getUserEntity());
        return schoolEntity.getId();
    }

    private void deleteAllStudentsFromSchool(StudentEntity studentToDelete){
        studentRepository.delete(studentToDelete);
        addressRepository.delete(studentToDelete.getAddress());
        usersRepository.delete(studentToDelete.getUserEntity());
    }

}
