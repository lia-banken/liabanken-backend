package com.example.liabanken.service;

import com.example.liabanken.exception.AdvertNotFoundException;
import com.example.liabanken.exception.StudentNotFoundException;
import com.example.liabanken.model.AdvertEntity;
import com.example.liabanken.model.StudentEntity;
import com.example.liabanken.repository.AddressRepository;
import com.example.liabanken.repository.AdvertRepository;
import com.example.liabanken.repository.StudentRepository;
import com.example.liabanken.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class StudentService {
    StudentRepository studentRepository;
    UsersRepository usersRepository;
    AdvertRepository advertRepository;
    AddressRepository addressRepository;

    public StudentEntity getStudentById(String studentId) throws StudentNotFoundException {
        return studentRepository.findById(studentId)
                .orElseThrow(() -> new StudentNotFoundException(studentId));
    }


    public Stream<StudentEntity> getAllStudents() {
        return studentRepository.findAll().stream();
    }

    public StudentEntity addFavorite(String studentId, String advertId) throws StudentNotFoundException, AdvertNotFoundException {
        StudentEntity studentEntity = studentRepository.findById(studentId).orElseThrow(() -> new StudentNotFoundException(studentId));
        AdvertEntity advertEntity = advertRepository.findById(advertId).orElseThrow(AdvertNotFoundException::new);
        studentEntity.addFavorite(advertEntity);
        studentRepository.save(studentEntity);
        return studentEntity;
    }

    public String createPassword(String studentId, String oldPassword, String newPassword) throws StudentNotFoundException {
        StudentEntity studentEntity = studentRepository.findById(studentId).orElseThrow(() -> new StudentNotFoundException(studentId));
        if (!studentEntity.getUserEntity().getPassword().equals(oldPassword)) {
            throw new IllegalStateException("password existeras inte");
        }
        studentEntity.getUserEntity().setPassword(newPassword);
        studentRepository.save(studentEntity);
        usersRepository.save(studentEntity.getUserEntity());
        return studentId;
    }

    public Stream<AdvertEntity> getAllFavoritesById(String studentId) throws StudentNotFoundException {
        StudentEntity studentEntity = studentRepository.findById(studentId).orElseThrow(() -> new StudentNotFoundException(studentId));
        return studentEntity.getFavorites().stream();
    }

    public Stream<AdvertEntity> getAllAdverts() {
        return advertRepository.findAll().stream();
    }

    public Stream<AdvertEntity> searchAdverts(String text) {
        return advertRepository.findAll().stream().filter(a -> a.getProfession().contains(text));
    }

    public StudentEntity updateProfile(String studentId, String firstname, String lastname) throws StudentNotFoundException {
        StudentEntity studentEntity = studentRepository.findById(studentId).orElseThrow(() -> new StudentNotFoundException(studentId));
        studentEntity.setFirstName(firstname);
        studentEntity.setLastName(lastname);
        studentRepository.save(studentEntity);
        return studentEntity;
    }

    public String removeStudentById(String studentId) throws StudentNotFoundException {
        StudentEntity studentEntity = studentRepository.findById(studentId).orElseThrow(() -> new StudentNotFoundException(studentId));
        studentRepository.deleteById(studentEntity.getId());
        usersRepository.delete(studentEntity.getUserEntity());
        addressRepository.delete(studentEntity.getAddress());
        return studentId;
    }

    public StudentEntity removeFavoriteById(String studentId, String advertId) throws StudentNotFoundException {
        StudentEntity studentEntity = studentRepository.findById(studentId).orElseThrow(() -> new StudentNotFoundException(studentId));
        List<AdvertEntity> collectAdvertList = studentEntity.getFavorites()
                .stream()
                .filter(e -> !e.getId().equals(advertId))
                .collect(Collectors.toList());
        studentEntity.setFavorites(collectAdvertList);
        studentRepository.save(studentEntity);
        return studentEntity;
    }

   /* public StudentEntity getStudentByKeycloakId(String keyCloakId) {
        return studentRepository.getStudentEntityByKeyCloakId(keyCloakId);

    }*/
}
