package com.example.liabanken.service;

import com.example.liabanken.exception.CreateUserFailException;
import com.example.liabanken.exception.UserNotFoundException;
import com.example.liabanken.jwt.KeyCloakToken;
import com.example.liabanken.keycloakCreateUser.KeycloakAdminClientService;
import com.example.liabanken.model.*;
import com.example.liabanken.repository.*;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class AdminService {

    AdminRepository adminRepository;
    AddressRepository addressRepository;
    SchoolRepository schoolRepository;
    UsersRepository usersRepository;
    TokenJwtRepository tokenJwtRepository;
    KeyCloakToken keyCloakToken;
    @Autowired
    KeycloakAdminClientService keycloakAdminClientService;


    public AdminService(AdminRepository adminRepository, AddressRepository addressRepository,
                        SchoolRepository schoolRepository, UsersRepository usersRepository,
                        TokenJwtRepository tokenJwtRepository) {
        this.adminRepository = adminRepository;
        this.addressRepository = addressRepository;
        this.schoolRepository = schoolRepository;
        this.usersRepository = usersRepository;
        this.tokenJwtRepository = tokenJwtRepository;
    }

    WebClient webClient;

    public AdminEntity createAdmin(String email, String password, String firstName, String lastName, Principal principal) throws CreateUserFailException {
      /*  String token;
        token = keyCloakToken.getAccessToken();
        webClient =  WebClient.builder()
                .baseUrl("http://localhost:8080")
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .build();*/

        AddressEntity addressEntity = new AddressEntity(UUID.randomUUID().toString(), "street", "zipcode", "city");
        UserEntity userEntity = new UserEntity(UUID.randomUUID().toString(), email, password, Role.ADMIN);
        AdminEntity adminEntity = new AdminEntity(UUID.randomUUID().toString(), principal.getName(), firstName, lastName, userEntity, addressEntity);
        addressRepository.save(addressEntity);
        usersRepository.save(userEntity);
        adminRepository.save(adminEntity);
        return adminEntity;
    }

    public boolean registerNewAdmin(String firstName, String lastName, String email, String userName, String password) {

        UserRepresentation user = keycloakAdminClientService.createKeycloakUser(firstName, lastName, email, userName, password);
        if (user == null)
            return false;
        System.out.println("UserRepresentation:  " + user);

        AddressEntity addressEntity = new AddressEntity(UUID.randomUUID().toString(), "street", "zipcode", "city");
        UserEntity userEntity = new UserEntity(UUID.randomUUID().toString(), email, password, Role.ADMIN);
        AdminEntity adminEntity = new AdminEntity(UUID.randomUUID().toString(),
                user.getId(),
                firstName,
                lastName,
                userEntity,
                addressEntity);

        System.out.println("nya adminEntity's KeyCloakId:  " + adminEntity.getKeyCloakId());
        addressRepository.save(addressEntity);
        usersRepository.save(userEntity);
        adminRepository.save(adminEntity);

        return true;
    }

    public Stream<AdminEntity> getAllAdmins() {
        return adminRepository.findAll().stream();
    }

    public AdminEntity getAdminById(String adminId) throws UserNotFoundException {
        return adminRepository.getAdminEntitiesByKeyCloakId(adminId);
    }

    public Stream<SchoolEntity> getAllSchoolRequests() {
        return schoolRepository.findAll().stream()
                .filter(schoolEntity -> !schoolEntity.isApproved());
    }

    public Mono<KeyCloakToken> acquire(String keyCloakBaseUrl, String realm, String clientId, String username, String password) {
        WebClient webClient = WebClient.builder()
                .baseUrl(keyCloakBaseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();
        return webClient.post()
                .uri("auth/realms/" + realm + "/protocol/openid-connect/token")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("grant_type", "password")
                        .with("client_id", clientId)
                        .with("username", username)
                        .with("password", password)
                        .with("access_token", ""))
                .retrieve()
                .bodyToFlux(KeyCloakToken.class)
                .onErrorMap(e -> new Exception("Failed to aquire token", e))
                .last();

    }

    public String getJwsToken() {
        keyCloakToken = acquire("http://localhost:8000/", "test", "test-student", "team4", "admin")
                .block();
        System.out.println(keyCloakToken.getAccessToken());
        return keyCloakToken.getAccessToken();
    }
/*fgnhdfghtyjtyjtyjtyjtyrtyhjrtjhrtjrtjrjtttyjdfh*/
}
