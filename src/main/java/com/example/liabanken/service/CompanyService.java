package com.example.liabanken.service;

import com.example.liabanken.exception.CompanyNotFoundException;
import com.example.liabanken.model.*;
import com.example.liabanken.repository.AddressRepository;
import com.example.liabanken.repository.AdvertRepository;
import com.example.liabanken.repository.CompanyRepository;
import com.example.liabanken.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class CompanyService {
    CompanyRepository companyRepository;
    AddressRepository addressRepository;
    UsersRepository usersRepository;
    AdvertRepository advertRepository;

    public CompanyEntity createCompany(String email,
                                       String password,
                                       String companyName,
                                       String organizationNumber,
                                       String street,
                                       String zipCode,
                                       String city) {
        AddressEntity addressEntity = new AddressEntity(UUID.randomUUID().toString(), street, zipCode, city);
        UserEntity userEntity = new UserEntity(UUID.randomUUID().toString(), email, password, Role.COMPANYREPRESENTATIVE);
        CompanyEntity companyEntity = new CompanyEntity(
                UUID.randomUUID().toString(),
                companyName,
                organizationNumber,
                addressEntity,
                userEntity,
                null,
                true,// Remember set to false
                new ArrayList<>());
        addressRepository.save(addressEntity);
        usersRepository.save(userEntity);
        companyRepository.save(companyEntity);
        return companyEntity;
    }

    public CompanyEntity getCompanyById(String id) throws CompanyNotFoundException {
        return companyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
    }

    public Stream<CompanyEntity> getAllApprovedCompanies() {
        return companyRepository.findAll().stream().filter(CompanyEntity::isApprove);
    }

    public Stream<CompanyEntity> getAllCompanyRequests() {
        return companyRepository.findAll()
                .stream()
                .filter(companyEntity -> !companyEntity.isApprove());
    }

    public Stream<CompanyEntity> getAllByName(String searchString) {
        Stream<CompanyEntity> companyEntityStream = companyRepository.findAll().stream();
        if (searchString != null && !searchString.equals(""))
            companyEntityStream = companyEntityStream.filter(searchByString(searchString));
        return companyEntityStream;
    }

    private Predicate<CompanyEntity> searchByString(String searchString) {
        final String searchStringToLowerCase = searchString.toLowerCase();
        return companyEntity -> {
            return companyEntity.getCompanyName().toLowerCase().contains(searchString) ||
                    companyEntity.getAddress().toString().contains(searchString);
        };
    }

    public CompanyEntity addAdvert(String companyId, String name, String description, String area, String profession) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        AdvertEntity advertEntity = new AdvertEntity(UUID.randomUUID().toString(), name, description, area, profession, false);
        companyEntity.addAdvert(advertEntity);
        advertRepository.save(advertEntity);
        companyRepository.save(companyEntity);
        return companyEntity;
    }

    public CompanyEntity createPassword(String companyId, String password) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        companyEntity.getUserEntity().setPassword(password);
        companyRepository.save(companyEntity);
        return companyEntity;
    }

    public CompanyEntity updateProfile(String companyId, String companyName, String orgNumber, String addressStreet, String zipCode, String city) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        companyEntity.setCompanyName(companyName);
        companyEntity.setOrganizationNumber(orgNumber);
        companyEntity.getAddress().setStreet(addressStreet);
        companyEntity.getAddress().setZipCode(zipCode);
        companyEntity.getAddress().setCity(city);
        companyRepository.save(companyEntity);
        return companyEntity;
    }

    public String removeCompany(String companyId) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyRepository.findById(companyId).orElseThrow(CompanyNotFoundException::new);
        companyEntity.getListOfAdverts().forEach(advertEntity -> advertRepository.deleteById(advertEntity.getId()));
        companyRepository.delete(companyEntity);
        addressRepository.delete(companyEntity.getAddress());
        usersRepository.delete(companyEntity.getUserEntity());
        return companyEntity.getId();
    }
}
