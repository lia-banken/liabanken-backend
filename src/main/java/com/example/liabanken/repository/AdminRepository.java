package com.example.liabanken.repository;
import com.example.liabanken.model.AdminEntity;
import com.example.liabanken.model.StudentEntity;
import com.example.liabanken.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminRepository extends JpaRepository<AdminEntity, String> {

  AdminEntity getAdminEntitiesByKeyCloakId(String keyCloakId);

  /*@Query("SELECT s FROM users s WHERE s.email = ?1")
  Optional<UserEntity> findAdminByEmail(String email);

  @Query("SELECT s FROM users s WHERE s.email = ?1")
  Optional<UserEntity> findAdminByPassword(String email);*/
}
