package com.example.liabanken.repository;

import com.example.liabanken.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<UserEntity, String> {

      @Query("SELECT s FROM users s WHERE s.email = ?1")
 public UserEntity findAdminByEmail(String email);

  @Query("SELECT s FROM users s WHERE s.password = ?1")
  Optional<UserEntity> findAdminByPassword(String password);
}
