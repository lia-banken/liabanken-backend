package com.example.liabanken.repository;

import com.example.liabanken.model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, String> {
    /*StudentEntity getStudentEntityByKeyCloakId(String keyCloakId);*/
}
