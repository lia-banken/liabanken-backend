package com.example.liabanken.repository;

import com.example.liabanken.model.AdvertEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertRepository extends JpaRepository<AdvertEntity, String> {
}
