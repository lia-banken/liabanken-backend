package com.example.liabanken.repository;

import com.example.liabanken.security.TokenJwt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenJwtRepository extends JpaRepository<TokenJwt, String> {
}
