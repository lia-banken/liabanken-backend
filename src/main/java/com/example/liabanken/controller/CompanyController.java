package com.example.liabanken.controller;

import com.example.liabanken.config.ConvertEntityToDTOs;
import com.example.liabanken.exception.CompanyNotFoundException;
import com.example.liabanken.model.CompanyEntity;
import com.example.liabanken.model.dto.CompanyDTO;
import com.example.liabanken.service.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@CrossOrigin
@RequestMapping("/company")
public class CompanyController {
    CompanyService companyService;

    @GetMapping("/all-approved-companies")
    public List<CompanyDTO> getAllApprovedCompanies() {
        return companyService.getAllApprovedCompanies()
                .map(ConvertEntityToDTOs::convertCompanyEntityToDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/company-by-id/{companyId}")
    public CompanyDTO getCompanyById(@PathVariable String companyId) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyService.getCompanyById(companyId);
        return ConvertEntityToDTOs.convertCompanyEntityToDTO(companyEntity);
    }

    @PostMapping("/create-company")
    public CompanyDTO createCompany(@RequestParam String email,
                                    @RequestParam String password,
                                    @RequestParam String companyName,
                                    @RequestParam String organizationNumber,
                                    @RequestParam String street,
                                    @RequestParam String zipcode,
                                    @RequestParam String city) {
        CompanyEntity companyEntity = companyService.createCompany(email, password, companyName, organizationNumber, street, zipcode, city);
        return ConvertEntityToDTOs.convertCompanyEntityToDTO(companyEntity);
    }

    @PostMapping("/add-advert/{companyId}")
    public CompanyDTO addAdvert(
            @PathVariable String companyId,
            @RequestParam String name,
            @RequestParam String description,
            @RequestParam String area,
            @RequestParam String profession) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyService.addAdvert(companyId, name, description, area, profession);
        return ConvertEntityToDTOs.convertCompanyEntityToDTO(companyEntity);
    }

    @PutMapping("/create-password/{companyId}")
    public CompanyDTO createPassword(@PathVariable String companyId, @RequestParam String password) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyService.createPassword(companyId, password);
        return ConvertEntityToDTOs.convertCompanyEntityToDTO(companyEntity);
    }

    @PutMapping("/update-profile/{companyId}")
    public CompanyDTO updateProfile(
            @PathVariable String companyId,
            @RequestParam String companyName,
            @RequestParam String orgNumber,
            @RequestParam String addressStreet,
            @RequestParam String zipCode,
            @RequestParam String city) throws CompanyNotFoundException {
        CompanyEntity companyEntity = companyService.updateProfile(companyId, companyName, orgNumber, addressStreet, zipCode, city);
        return ConvertEntityToDTOs.convertCompanyEntityToDTO(companyEntity);
    }

    @DeleteMapping("/remove-company")
    public String removeCompany(@RequestParam String companyId) throws CompanyNotFoundException {
        return companyService.removeCompany(companyId);
    }

}
