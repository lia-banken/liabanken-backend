package com.example.liabanken.controller;

import com.example.liabanken.config.ConvertEntityToDTOs;
import com.example.liabanken.exception.CompanyNotFoundException;
import com.example.liabanken.exception.SchoolNotFoundException;
import com.example.liabanken.exception.StudentNotFoundException;
import com.example.liabanken.model.SchoolEntity;
import com.example.liabanken.model.StudentEntity;
import com.example.liabanken.model.dto.SchoolDTO;
import com.example.liabanken.model.dto.StudentDTO;
import com.example.liabanken.service.SchoolService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin
@AllArgsConstructor
@RequestMapping("/school")
public class SchoolController {
    SchoolService schoolService;

    @GetMapping("/all-approved-schools")
    public List<SchoolDTO> getAllApprovedSchools() {
        return schoolService.getAllApprovedSchools()
                .map(ConvertEntityToDTOs::convertSchoolEntityToDTO).collect(Collectors.toList());
    }

    @GetMapping("/get-approve-school-by-id/{schoolId}")
    public SchoolDTO getApprovedSchoolById(@PathVariable(value = "schoolId") String schoolId) throws SchoolNotFoundException {
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolService.getApprovedSchoolById(schoolId));
    }

    @GetMapping("/school-request-by-id/{id}")
    public SchoolDTO getSchoolRequestById(@PathVariable String id) throws SchoolNotFoundException {
        SchoolEntity schoolRequestById = schoolService.getSchoolRequestById(id);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolRequestById);
    }

    @GetMapping("/get-school/{schoolId}")
    public SchoolDTO getSchoolById(@PathVariable(value = "schoolId") String schoolId) throws SchoolNotFoundException {
        SchoolEntity schoolRequestById = schoolService.getSchoolById(schoolId);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolRequestById);
    }

    @GetMapping("/student/{schoolId}")
    public Stream<StudentDTO> getAllStudentBySchoolId(@PathVariable String schoolId) throws SchoolNotFoundException {
        Stream<StudentEntity> allStudentsBySchoolId = schoolService.getAllStudentsBySchoolId(schoolId);
        return allStudentsBySchoolId.map(ConvertEntityToDTOs::convertStudentEntityToDTO);
    }

    @PostMapping("/approve-company-by-school")
    public SchoolDTO addCompanyToSchool(@RequestParam String schoolId,
                                        @RequestParam String companyId) throws SchoolNotFoundException, CompanyNotFoundException {
        SchoolEntity schoolEntity = schoolService.approveCompanyBySchoolId(schoolId, companyId);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolEntity);
    }

    @PostMapping("/add-student")
    public SchoolDTO addStudentToSchool(@RequestParam String schoolId,
                                        @RequestParam String password,
                                        @RequestParam String firstname,
                                        @RequestParam String lastname,
                                        @RequestParam String email,
                                        @RequestParam String street,
                                        @RequestParam String zipcode,
                                        @RequestParam String city) throws SchoolNotFoundException, CompanyNotFoundException {
        SchoolEntity student = schoolService.createStudent(schoolId, password, firstname, lastname, email, street, zipcode, city);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(student);
    }

    @PostMapping("/create-school")
    public SchoolDTO createSchool(@RequestParam String email,
                                  @RequestParam String schoolName,
                                  @RequestParam String organizationNumber,
                                  @RequestParam String street,
                                  @RequestParam String zipcode,
                                  @RequestParam String city) {
        SchoolEntity newSchool = schoolService.createSchool(email, schoolName, organizationNumber, street, zipcode, city);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(newSchool);
    }

    @PostMapping("/create-password/{id}")
    public SchoolDTO createPassword(@PathVariable String id,
                                    @RequestParam String email,
                                    @RequestParam String password) {
        SchoolEntity schoolUserPasswordChange = schoolService.createPassword(id, email, password);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolUserPasswordChange);
    }

    @DeleteMapping("/remove-company")
    public SchoolDTO removeCompanyFromSchool(@RequestParam String schoolId,
                                             @RequestParam String companyId) throws CompanyNotFoundException, SchoolNotFoundException {
        SchoolEntity schoolEntity = schoolService.removeCompanyFromSchool(schoolId, companyId);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolEntity);
    }

    @DeleteMapping("/remove-student")
    public SchoolDTO removeStudentFromSchool(@RequestParam String schoolId, @RequestParam String studentId) throws StudentNotFoundException, SchoolNotFoundException {
        SchoolEntity schoolEntity = schoolService.removeStudentFromSchool(schoolId, studentId);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolEntity);
    }

    @DeleteMapping("/remove-school/{schoolId}")
    public String removeSchoolById(@PathVariable String schoolId) throws SchoolNotFoundException {
        return schoolService.removeSchoolById(schoolId);
    }
}

