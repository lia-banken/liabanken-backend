package com.example.liabanken.controller;

import com.example.liabanken.config.ConvertEntityToDTOs;
import com.example.liabanken.exception.AdvertNotFoundException;
import com.example.liabanken.exception.StudentNotFoundException;
import com.example.liabanken.model.AdvertEntity;
import com.example.liabanken.model.CompanyEntity;
import com.example.liabanken.model.StudentEntity;
import com.example.liabanken.model.dto.AdvertDTO;
import com.example.liabanken.model.dto.CompanyDTO;
import com.example.liabanken.model.dto.StudentDTO;
import com.example.liabanken.service.CompanyService;
import com.example.liabanken.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Stream;

@CrossOrigin("*")
@RestController
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {
    StudentService studentService;
    CompanyService companyService;

    @GetMapping("/all")
    public Stream<StudentDTO> getAllStudents() {
        return studentService.getAllStudents().map(ConvertEntityToDTOs::convertStudentEntityToDTO);
    }

  /*  @GetMapping("/own")
    public StudentDTO getStudentById( Principal principal)  {
        System.out.println("StudentController.getStudentById " + principal.getName());
        StudentEntity studentById = studentService.getStudentByKeycloakId(principal.getName());
        System.out.println(studentById.getFirstName());
        return ConvertEntityToDTOs.convertStudentEntityToDTO(studentById);
    }*/

    @GetMapping("/{studentId}")
    public StudentDTO getStudentById(@PathVariable String studentId) throws StudentNotFoundException {
        StudentEntity studentById = studentService.getStudentById(studentId);
        return ConvertEntityToDTOs.convertStudentEntityToDTO(studentById);
    }

    @GetMapping("/all-companies/")
    public Stream<CompanyDTO> getAllApprovedCompanies() {
        Stream<CompanyEntity> allApprovedCompanies = companyService.getAllApprovedCompanies();
        return allApprovedCompanies.map(ConvertEntityToDTOs::convertCompanyEntityToDTO);
    }

    @GetMapping("/all-favorites/{studentId}")
    public Stream<AdvertDTO> getAllFavoritesById(@PathVariable String studentId) throws StudentNotFoundException {
        Stream<AdvertEntity> allFavoritesById = studentService.getAllFavoritesById(studentId);
        return allFavoritesById.map(ConvertEntityToDTOs::convertAdvertEntityToDto);
    }

    @GetMapping("/all-adverts/")
    public Stream<AdvertDTO> getAllAdverts() {
        Stream<AdvertEntity> allAdverts = studentService.getAllAdverts();
        return allAdverts.map(ConvertEntityToDTOs::convertAdvertEntityToDto);
    }

    @GetMapping("/search-advert/")
    public Stream<AdvertDTO> searchAdverts(@RequestParam String text) {
        Stream<AdvertEntity> advertEntityStream = studentService.searchAdverts(text);
        return advertEntityStream.map(ConvertEntityToDTOs::convertAdvertEntityToDto);
    }

    @PutMapping("/create-password/{studentId}")
    public String createPassword(@PathVariable String studentId, @RequestParam String oldPassword, @RequestParam String newPassword) throws StudentNotFoundException {
        return studentService.createPassword(studentId, oldPassword, newPassword);
    }

    @PutMapping("/update-profile/{studentId}")
    public StudentDTO updateProfile(@PathVariable String studentId,
                                    @RequestParam String firstname,
                                    @RequestParam String lastname) throws StudentNotFoundException {
        StudentEntity studentEntity = studentService.updateProfile(studentId, firstname, lastname);
        return ConvertEntityToDTOs.convertStudentEntityToDTO(studentEntity);
    }

    @PutMapping("/add-favorite/{studentId}")
    public StudentDTO addFavorite(@PathVariable String studentId, @RequestParam String advertId) throws StudentNotFoundException, AdvertNotFoundException {
        StudentEntity studentEntity = studentService.addFavorite(studentId, advertId);
        return ConvertEntityToDTOs.convertStudentEntityToDTO(studentEntity);
    }

    @DeleteMapping("/remove-student/{studentId}")
    public String removeStudentById(@PathVariable String studentId) throws StudentNotFoundException {
        return studentService.removeStudentById(studentId);
    }

    @DeleteMapping("/remove-student-favorite/{studentId}")
    public StudentDTO removeFavorite(@PathVariable String studentId, @RequestParam String advertId) throws StudentNotFoundException {
        StudentEntity studentEntity = studentService.removeFavoriteById(studentId, advertId);
        return ConvertEntityToDTOs.convertStudentEntityToDTO(studentEntity);
    }
}
