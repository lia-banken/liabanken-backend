package com.example.liabanken.controller;

import com.example.liabanken.config.ConvertEntityToDTOs;
import com.example.liabanken.exception.AddressNotFoundException;
import com.example.liabanken.exception.CreateUserFailException;
import com.example.liabanken.exception.SchoolNotFoundException;
import com.example.liabanken.exception.UserNotFoundException;
import com.example.liabanken.model.AdminEntity;
import com.example.liabanken.model.SchoolEntity;
import com.example.liabanken.model.dto.AdminDTO;
import com.example.liabanken.model.dto.SchoolDTO;
import com.example.liabanken.service.AdminService;
import com.example.liabanken.service.SchoolService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.stream.Stream;

@Slf4j
@RestController
@AllArgsConstructor
@CrossOrigin()
@RequestMapping("/admin")
@RolesAllowed("admin")
public class AdminController {

    AdminService adminService;
    SchoolService schoolService;
/*ryhryjryjryjry*/

    @GetMapping("/createJsonWebToken")
    public String keycloakId() {
        return adminService.getJwsToken();
    }

    @GetMapping("/all")
    public Stream<AdminDTO> getAllAdmins() {
        return adminService.getAllAdmins().map(ConvertEntityToDTOs::convertAdminDTO);
    }

    @GetMapping("/get_By_Id")
    public AdminDTO getAdminById(Principal principal) throws UserNotFoundException {
        System.out.println("hello");
        AdminEntity adminById = adminService.getAdminById(principal.getName());
        return ConvertEntityToDTOs.convertAdminDTO(adminById);
    }

    @GetMapping("/all-school-requests")
    public Stream<SchoolDTO> getAllASchoolRequests(/*Principal principal*/) {
        return adminService.getAllSchoolRequests()
                .map(ConvertEntityToDTOs::convertSchoolEntityToDTO);
    }

    // @RolesAllowed("admin")
    @PostMapping("/create-admin")
    public AdminDTO createAdminUser(
            @RequestParam String email,
            @RequestParam String password,
            @RequestParam String firstName,
            @RequestParam String lastName,
            Principal principal)
            throws CreateUserFailException {
        AdminEntity adminEntity = adminService.createAdmin(email, password, firstName, lastName, principal);
        return ConvertEntityToDTOs.convertAdminDTO(adminEntity);
    }

    @GetMapping("/register_new_admin_user")
    public ResponseEntity registerNewAdminUser(@RequestParam(value = "firstName") String firstName,
                                               @RequestParam(value = "lastName") String lastName,
                                               @RequestParam(value = "email") String email,
                                               @RequestParam(value = "userName") String userName,
                                               @RequestParam(value = "password") String password) {
        boolean success = adminService.registerNewAdmin(firstName, lastName, email, userName, password);
        return success ? ResponseEntity.ok("User created") : ResponseEntity.notFound().build();
    }

    @PostMapping("/approve-school-requests")
    public SchoolDTO addSchoolFromRequest(@RequestParam String schoolId) throws SchoolNotFoundException {
        SchoolEntity schoolEntity = schoolService.approveSchoolById(schoolId);
        return ConvertEntityToDTOs.convertSchoolEntityToDTO(schoolEntity);
    }

    @DeleteMapping("/delete-school-requests")
    public String deleteSchoolFromRequest(@RequestParam String schoolId) throws SchoolNotFoundException, AddressNotFoundException {
        return schoolService.removeSchoolFromRequest(schoolId);
    }

}